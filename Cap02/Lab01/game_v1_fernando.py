# Game Ping-Pong

from tkinter import * # Importa tudo do modulo de interface grafica tkinter.
import random # Importa modulo random, de geracao de numeros aleatorios.
import time # Importa modulo de funcoes associadas a 'tempo'.


######################### Inicio

# Abaixo, comando 'input' gera um texto no terminal e grava uma resposta no terminal.
# O comando 'int' converte em inteiro, em seguida, eh gravado na variavel 'level.
level = int(input("Qual nível você gostaria de jogar? 1/2/3/4/5 \n"))
length = 500/level # Defini-se o comprimento da barra, relacionado ao nivel de
                   # dificuldade do jogo.

root = Tk() # Cria uma instancia de classe do módulo tkinter, a janela principal.
root.title("Ping Pong") # Executa o metodo que define o titulo da janela. 
root.resizable(0,0) # Apĺica metodo que permite mudar tamanho da janela
root.wm_attributes("-topmost", -1) #Este metodo define mais atributos da janela. 
                                   #A opcao '-topmost' determina que a janela sempre
                                   #estara a frente de todas as outras. Recurso
                                   #comum em games. O parametro '-1' nao achei.

# Abaixo, cria uma insancia da classe Canvas. Ela eh do tipo widget,
# que fornece facilidades graficas.
canvas = Canvas(root, width=800, height=600, bd=0,highlightthickness=0)
canvas.pack() # Este metodo organiza geometricamente o widget.

root.update() # Nao entendi porque ha a necessidade de 'atualizacao' da instancia
              # root (janela).

# Variável
count = 0
lost = False


######################### Classes Bola e Barra

class Bola:
    """Modela a bola do jogo."""
    def __init__(self, canvas, Barra, color):
        self.canvas = canvas
        self.Barra = Barra
        self.id = canvas.create_oval(0, 0, 15, 15, fill=color)
        self.canvas.move(self.id, 245, 200)

        starts_x = [-3, -2, -1, 1, 2, 3]
        random.shuffle(starts_x)

        self.x = starts_x[0]
        self.y = -3

        self.canvas_height = self.canvas.winfo_height()
        self.canvas_width = self.canvas.winfo_width()


    def draw(self):
        self.canvas.move(self.id, self.x, self.y)

        pos = self.canvas.coords(self.id)

        if pos[1] <= 0:
            self.y = 3

        if pos[3] >= self.canvas_height:
            self.y = -3

        if pos[0] <= 0:
            self.x = 3
            
        if pos[2] >= self.canvas_width:
            self.x = -3

        self.Barra_pos = self.canvas.coords(self.Barra.id)


        if pos[2] >= self.Barra_pos[0] and pos[0] <= self.Barra_pos[2]:
            if pos[3] >= self.Barra_pos[1] and pos[3] <= self.Barra_pos[3]:
                self.y = -3
                global count
                count +=1
                score() # Funcao definida mais abaixo.


        if pos[3] <= self.canvas_height:
            self.canvas.after(10, self.draw)
        else:
            game_over() # Funcao definida mais abaixo.
            global lost
            lost = True


class Barra:
    """Modela a barra do jogo."""
    def __init__(self, canvas, color):
        self.canvas = canvas
        self.id = canvas.create_rectangle(0, 0, length, 10, fill=color)
        self.canvas.move(self.id, 200, 400)

        self.x = 0

        self.canvas_width = self.canvas.winfo_width()

        self.canvas.bind_all("<KeyPress-Left>", self.move_left)
        self.canvas.bind_all("<KeyPress-Right>", self.move_right)

    def draw(self):
        self.canvas.move(self.id, self.x, 0)

        self.pos = self.canvas.coords(self.id)

        if self.pos[0] <= 0:
            self.x = 0
        
        if self.pos[2] >= self.canvas_width:
            self.x = 0
        
        global lost
        
        if lost == False:
            self.canvas.after(10, self.draw)

    def move_left(self, event):
        if self.pos[0] >= 0:
            self.x = -3

    def move_right(self, event):
        if self.pos[2] <= self.canvas_width:
            self.x = 3

################################################### Funcoes

def start_game(event):
    """Funcao que inicia o jogo."""
    global lost, count
    lost = False
    count = 0
    score() # Executa a funcao score() definida abaixo.
    canvas.itemconfig(game, text=" ")

    time.sleep(1)
    Barra.draw()
    Bola.draw()


def score():
    """Funcao que mostra a pontuacao."""
    canvas.itemconfig(score_now, text="Pontos: " + str(count))

def game_over():
    """Funcao que mostra mensagem de 'Game over!'. """
    canvas.itemconfig(game, text="Game over!")

######################################### Principal

# Instaciacao 'Barra' e 'Bola' e definicoes de mais alguns parametros.
Barra = Barra(canvas, "orange") #Cria a instancia 'Barra' definindo a cor da barra.
Bola = Bola(canvas, Barra, "purple") # Cria instancia 'Bola' com a cor da bola.

score_now = canvas.create_text(430, 20, text="Pontos: " + str(count), fill = "green", font=("Arial", 16)) #Deve mostrar a pontuacao na tela.
game = canvas.create_text(400, 300, text=" ", fill="red", font=("Arial", 40))
                    #Deve ser a mensagem de game over em vermelho.


### Execucao principal que roda o jogo, onde a funcao 'start_game' eh chamada.
canvas.bind_all("<Button-1>", start_game) # Executa a funcao 'start_game' pelo
                                          # 'botao 1' do mouse, ligando a instancia
                                          # canvas pelo metodo '.bind_all()'.

root.mainloop() #Execucao principal da janela 'root' onde rodara o jogo.
