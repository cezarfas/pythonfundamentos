#!/usr/bin/env python
# coding: utf-8

# # <font color='blue'>Data Science Academy - Python Fundamentos - Capítulo 2</font>
# 
# ## Download: http://github.com/dsacademybr

# In[ ]:


# Versão da Linguagem Python
from platform import python_version
print('Versão da Linguagem Python Usada Neste Jupyter Notebook:', python_version())


# ## Exercícios Cap02

# In[ ]:


# Exercício 1 - Imprima na tela os números de 1 a 10. Use uma lista para armazenar os números.
dezena = list(range(1,11)) 
print("\n\n Exercício 1:\n")
print(dezena)


# In[ ]:


# Exercício 2 - Crie uma lista de 5 objetos e imprima na tela
minha_lista = [32.3, 86, 'bahia', ['salvador', 'recife'], 0.50]
print("\n\n Exercício 2:\n")
print(minha_lista)



# In[ ]:


# Exercício 3 - Crie duas strings e concatene as duas em uma terceira string

nome = 'José '
sobrenome = 'Bonifácio'
completo = nome + sobrenome
print("\n\n Exercício 3:\n")
print(completo)


# In[ ]:


# Exercício 4 - Crie uma tupla com os seguintes elementos: 1, 2, 2, 3, 4, 4, 4, 5 e depois utilize a função count do 
# objeto tupla para verificar quantas vezes o número 4 aparece na tupla

lista1 = (1, 2, 2, 3, 4, 4, 4, 5)
print("\n\n Exercício 4:\n")
print(lista1.count(4))



# In[ ]:


# Exercício 5 - Crie um dicionário vazio e imprima na tela
dic1 = {}
print("\n\n Exercício 5:\n")
print(dic1)


# In[ ]:


# Exercício 6 - Crie um dicionário com 3 chaves e 3 valores e imprima na tela
dic1 = {'mon1':'D. João VI','mon2':'D. Pedro I','mon3':'D. Pedro II'}
print("\n\n Exercício 6:\n")
print(dic1)



# In[ ]:


# Exercício 7 - Adicione mais um elemento ao dicionário criado no exercício anterior e imprima na tela
dic1['mon4'] = 'P. Isabel'
print("\n\n Exercício 7:\n")
print(dic1)


# In[ ]:


# Exercício 8 - Crie um dicionário com 3 chaves e 3 valores. Um dos valores deve ser uma lista de 2 elementos numéricos. 
# Imprima o dicionário na tela.
del dic1['mon4']
dic1['mon1'] = [1, 6]
print("\n\n Exercício 8:\n")
print(dic1)


# In[ ]:


# Exercício 9 - Crie uma lista de 4 elementos. O primeiro elemento deve ser uma string, 
# o segundo uma tupla de 2 elementos, o terceiro um dcionário com 2 chaves e 2 valores e 
# o quarto elemento um valor do tipo float.
# Imprima a lista na tela.
lista1 = ['títulos', (1959,1988), {'time':'bahia','ano':1930}, 50.0]
print("\n\n Exercício 9:\n")
print(lista1)


# In[ ]:


# Exercício 10 - Considere a string abaixo. Imprima na tela apenas os caracteres da posição 1 a 18.
frase = 'Cientista de Dados é o profissional mais sexy do século XXI'
print("\n\n Exercício 10:\n")
print(frase[0:18])


# # Fim

# ### Obrigado - Data Science Academy - <a href="http://facebook.com/dsacademybr">facebook.com/dsacademybr</a>
