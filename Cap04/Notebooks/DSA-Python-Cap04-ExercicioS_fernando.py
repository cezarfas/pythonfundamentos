#!/usr/bin/env python
# coding: utf-8

# # <font color='blue'>Data Science Academy - Python Fundamentos - Capítulo 4</font>
# 
# ## Download: http://github.com/dsacademybr

# In[ ]:


# Versão da Linguagem Python
from platform import python_version
print('Versão da Linguagem Python Usada Neste Jupyter Notebook:', python_version())


# ## Exercícios 

# In[ ]:


# Exercício 1 - Crie uma lista de 3 elementos e calcule a terceira potência de cada elemento.
x = [3.0, 4.0, 5.0]
y = list(map(lambda k: k**3.0 , x)) #Aplicacao das funcoes lambda e map
print('01)\n')
print(y, '\n')


# In[ ]:


# Exercício 2 - Reescreva o código abaixo, usando a função map(). O resultado final deve ser o mesmo!
palavras = 'A Data Science Academy oferce os melhores cursos de análise de dados do Brasil'.split()
resultado = [[w.upper(), w.lower(), len(w)] for w in palavras]
print('02)\n')
for i in resultado:
    print (i)
print('\n')

respostas = map(lambda w: [w.upper(), w.lower(), len(w)], palavras)

for resposta in respostas:
    print(resposta)
print('\n')


# In[ ]:


# Exercício 3 - Calcule a matriz transposta da matriz abaixo.
# Caso não saiba o que é matriz transposta, visite este link: https://pt.wikipedia.org/wiki/Matriz_transposta
# Matriz transposta é um conceito fundamental na construção de redes neurais artificiais, base de sistemas de IA.
matrix = [[1, 2],[3,4],[5,6],[7,8]]
tr_m = []
nl = len(matrix) # numero de linhas da matriz
nc = len(matrix[0]) # numero de colunas da matriz
for ic in range(nc): # Comeca por coluna pq a primeira coluna sera a primeira linha na
    linha = []       # matriz transposta.
    for il in range(nl):
        linha.append(matrix[il][ic]) # Constroi os elementos (coluna) de cada linha 
    tr_m.append(linha)               # da matriz transposta. E acrescenta a linha nela.
print('03)')
print(tr_m,'\n')

# In[ ]:


# Exercício 4 - Crie duas funções, uma para elevar um número ao quadrado e outra para elevar ao cubo. 
# Aplique as duas funções aos elementos da lista abaixo. 
# Obs: as duas funções devem ser aplicadas simultaneamente.
lista = [0, 1, 2, 3, 4]
elev2e3 = [[num**2, num**3] for num in lista] # Solucao eh com list comprehension
print('04)')
print(elev2e3,'\n')

# In[ ]:


# Exercício 5 - Abaixo você encontra duas listas. Faça com que cada elemento da listaA seja elevado 
# ao elemento correspondente na listaB.
listaA = [2, 3, 4]
listaB = [10, 11, 12]       #Mais uma solucao com list comprehension
eleva = [ [ listaA[k]**listaB[k] ]  for k in range(len(listaA)) ]
print('05)\n')
print(eleva,'\n')

# In[ ]:


# Exercício 6 - Considerando o range de valores abaixo, use a função filter() para retornar apenas os valores negativos.
ra = range(-5, 5)
numeros = list(ra)
nega = list(filter(lambda k: k < 0,numeros))
print('06)')
print(nega,'\n')


# In[ ]:


# Exercício 7 - Usando a função filter(), encontre os valores que são comuns às duas listas abaixo.
a = [1,2,3,5,7,9]
b = [2,3,5,6,7,8]
comum = list(filter(lambda k: k in b, a))
print('07)')
print(comum,'\n')

# In[ ]:


# Exercício 8 - Considere o código abaixo. Obtenha o mesmo resultado usando o pacote time. 
# Não conhece o pacote time? Pesquise!
import datetime
print('08) \n')
print (datetime.datetime.now().strftime("%d/%m/%Y %H:%M"))

import time
named_tuple = time.localtime() # get struct_time
time_string = time.strftime("%d/%m/%Y %H:%M", named_tuple)

print(time_string,'\n')

# In[ ]:


# Exercício 9 - Considere os dois dicionários abaixo. 
# Crie um terceiro dicionário com as chaves do dicionário 1 e os valores do dicionário 2.
dict1 = {'a':1,'b':2}
dict2 = {'c':4,'d':5}
ks = list(dict1.keys())
vs = list(dict2.values())
dict3 = {}
conta = 0
for k in ks:
    dict3[k] = vs[conta]
    conta += 1

print('09)')
print(dict3,'\n')

# In[ ]:


# Exercício 10 - Considere a lista abaixo e retorne apenas os elementos cujo índice for maior que 5.
lista = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
nova = list(enumerate(lista))
resp = list(filter(lambda k: k[0]>5, nova))
print(resp,'\n')

# # Fim

# ### Obrigado - Data Science Academy - <a href="http://facebook.com/dsacademybr">facebook.com/dsacademybr</a>
