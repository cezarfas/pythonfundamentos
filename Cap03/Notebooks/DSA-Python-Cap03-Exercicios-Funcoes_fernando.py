#!/usr/bin/env python
# coding: utf-8

# # <font color='blue'>Data Science Academy - Python Fundamentos - Capítulo 3</font>
# 
# ## Download: http://github.com/dsacademybr

# In[ ]:


# Versão da Linguagem Python
from platform import python_version
print('Versão da Linguagem Python Usada Neste Jupyter Notebook:', python_version())


# ## Exercícios - Métodos e Funções

# In[ ]:


# Exercício 1 - Crie uma função que imprima a sequência de números pares entre 1 e 20 (a função não recebe parâmetro) e 
# depois faça uma chamada à função para listar os números      
def pares20():
    '''Identifica os pares.'''
    print('\n Numeros pares: ', list(range(2,21,2)), '\n')

pares20()

# In[ ]:


# Exercício 2 - Crie uam função que receba uma string como argumento e retorne a mesma string em letras maiúsculas.
# Faça uma chamada à função, passando como parâmetro uma string

def caixaAlta(texto):
    '''Transforma em caixa alta.'''
    texto = texto.upper()
    return texto

#entrada = input('Digite a string: ')
entrada = 'bolo'
print(caixaAlta(entrada),'\n')


# In[ ]:


# Exercício 3 - Crie uma função que receba como parâmetro uma lista de 4 elementos, adicione 2 elementos a lista e 
# imprima a lista

def adicCarros(carros):
    '''Adiciona novos carros na lista.'''
    novos_carros = ['Fusca','Voyage']
    carros.extend(novos_carros)
    print('Nova lista de carros: ', carros, '\n')

meus_carros = ['Clio', '207', 'Fit', 'Fit']
adicCarros(meus_carros)

# In[ ]:


# Exercício 4 - Crie uma função que receba um argumento formal e uma possível lista de elementos. Faça duas chamadas 
# à função, com apenas 1 elemento e na segunda chamada com 4 elementos

def pizzaSabores(sabor, *sabores):
    '''Sabores da pizza'''

    total = []
    total.append(sabor)
    total.extend(list(sabores)) # '*sabores' eh transformada numa tupla
    print('Pedido confirmado. Pizza escolhida: ', total, '\n' )

s1 = 'marguerita'
pizzaSabores(s1)
s2 = 'parma'
s3 = 'calabresa'
s4 = 'atum'
pizzaSabores(s1,s2,s3,s4)


# In[ ]:


# Exercício 5 - Crie uma função anônima e atribua seu retorno a uma variável chamada soma. A expressão vai receber 2 
# números como parâmetro e retornar a soma deles
soma = lambda a, b: a+b
total = soma(2,19)
print('Resultado da soma é ',total,'.\n')

# In[ ]:


# Exercício 6 - Execute o código abaixo e certifique-se que compreende a diferença entre variável global e local
total = 0
def soma( arg1, arg2 ):
    total = arg1 + arg2; 
    print ("Dentro da função o total é: ", total)
    return total;


soma( 10, 20 );
print ("Fora da função o total é: ", total)


# In[ ]:


# Exercício 7 - Abaixo você encontra uma lista com temperaturas em graus Celsius
# Crie uma função anônima que converta cada temperatura para Fahrenheit
# Dica: para conseguir realizar este exercício, você deve criar sua função lambda, dentro de uma função 
# (que será estudada no próximo capítulo). Isso permite aplicar sua função a cada elemento da lista
# Como descobrir a fórmula matemática que converte de Celsius para Fahrenheit? Pesquise!!!

Cel = [39.2, 36.5, 37.3, 37.8]
Fah = lambda c: c*1.8 + 32.0
Fahrenheit = map(Fah, Cel) #Como utilizar a funcao map()
print ('Temperaturas em oF: ', list(Fahrenheit), '\n')


# In[ ]:


# Exercício 8
# Crie um dicionário e liste todos os métodos e atributos do dicionário
d1 = {'ba':'bahia','se':'sergipe','sp':'sao paulo'}
saidas = dir(d1)

print('Os metodos e atributos do dicionário criado: ')
for saida in saidas:
    print(saida)

print('\n')

# In[ ]:


# Exercício 9
# Abaixo você encontra a importação do Pandas, um dos principais pacotes Python para análise de dados.
# Analise atentamente todos os métodos disponíveis. Um deles você vai usar no próximo exercício.

#import pandas as pd
#dir(pd)


# In[ ]:


# ************* Desafio ************* (pesquise na documentação Python)

# Exercício 10 - Crie uma função que receba o arquivo abaixo como argumento e retorne um resumo estatístico descritivo 
# do arquivo. Dica, use Pandas e um de seus métodos, describe()
# Arquivo: "binary.csv"
import pandas as pd
file_name = "binary_fernando.csv"

def resumoStat(arquivo):
    '''Resumo estatístico. '''
    dados = []
    dado_in = pd.read_csv(arquivo)  #Leitura pandas de arquivo 'csv'
    dado_out = dado_in.describe()   #Método par resumo estatístico.
    dados.append(dado_in) 
    dados.append(dado_out)
    return dados

saida = resumoStat(file_name)
print('Arquivo avaliado: \n', saida[0], '\n')
print('Resumo estatístico descritivo: \n', saida[1], '\n')

# # Fim

# ### Obrigado - Data Science Academy - <a href="http://facebook.com/dsacademybr">facebook.com/dsacademybr</a>
