#!/usr/bin/env python
# coding: utf-8

# # <font color='blue'>Data Science Academy - Python Fundamentos - Capítulo 3</font>
# 
# ## Download: http://github.com/dsacademybr

# In[ ]:


# Versão da Linguagem Python
from platform import python_version
print('Versão da Linguagem Python Usada Neste Jupyter Notebook:', python_version())


# ## Exercícios - Loops e Condiconais

# In[ ]:


# Exercício 1 - Crie uma estrutura que pergunte ao usuário qual o dia da semana. Se o dia for igual a Domingo ou 
# igual a sábado, imprima na tela "Hoje é dia de descanso", caso contrário imprima na tela "Você precisa trabalhar!"
dia = 'domingo'
#dia = input('Qual o dia da semana? ') # Esta opcao pergunta no terminal.
dia = dia.lower() #Garante todas as letras minusculas
dias = ['domingo','segunda','terça','quarta','quinta','sexta','sábado']
if dia in dias:
    if dia=='sábado' or dia=='domingo':
        print('\nHoje é dia de descanso.\n')
    else:
        print('\nVocê precisa trabalhar!\n')
else:
    print('\nEste dia não é válido ou não está escrito certo.\n')

# In[ ]:


# Exercício 2 - Crie uma lista de 5 frutas e verifique se a fruta 'Morango' faz parte da lista
frutas = ['Maçã', 'Banana', 'Melancia', 'Jaca', 'Laranja']
resp = 'Morango' in frutas
print(resp,'\n')

# In[ ]:


# Exercício 3 - Crie uma tupla de 4 elementos, multiplique cada elemento da tupla por 2 e guarde os resultados em uma 
# lista
esportes = (10,12,14,16)
esp2 = []
for esporte in esportes:
    esporte *= 2
    esp2.append(esporte)
print(esp2,'\n')

# In[ ]:


# Exercício 4 - Crie uma sequência de números pares entre 100 e 150 e imprima na tela
pares = []
for k in range(100,152,2):
    pares.append(k)
print(pares,'\n')



# In[ ]:


# Exercício 5 - Crie uma variável chamada temperatura e atribua o valor 40. Enquanto temperatura for maior que 35, 
# imprima as temperaturas na tela
temperatura = 40
while temperatura > 35:
    print(temperatura)
    temperatura -= 1
print('\n')

# In[ ]:


# Exercício 6 - Crie uma variável chamada contador = 0. Enquanto counter for menor que 100, imprima os valores na tela,
# mas quando for encontrado o valor 23, interrompa a execução do programa
contador = 0
while contador<100 :
    if contador==23:
        break
    print(contador)  
    contador += 1
print('\n')

# In[ ]:


# Exercício 7 - Crie uma lista vazia e uma variável com valor 4. Enquanto o valor da variável for menor ou igual a 20, 
# adicione à lista, apenas os valores pares e imprima a lista
pares = []
k = 4
while k<=20:
    pares.append(k)
    k+=2
print(pares,'\n')

# In[ ]:


# Exercício 8 - Transforme o resultado desta função range em uma lista: range(5, 45, 2)
nums = range(5, 45, 2)
print(list(nums),'\n')


# In[ ]:


# Exercício 9 - Faça a correção dos erros no código abaixo e execute o programa. Dica: são 3 erros.
#temperatura = float(input('Qual a temperatura? '))
temperatura=25
if temperatura > 30:
    print('Vista roupas leves.')
else:
    print('Busque seus casacos.')
print('\n')

# In[ ]:


# Exercício 10 - Faça um programa que conte quantas vezes a letra "r" aparece na frase abaixo. Use um placeholder na 
# sua instrução de impressão

# “É melhor, muito melhor, contentar-se com a realidade; se ela não é tão brilhante como os sonhos, tem pelo menos a 
# vantagem de existir.” (Machado de Assis)

frase = "É melhor, muito melhor, contentar-se com a realidade; se ela não é tão brilhante como os sonhos, tem pelo menos a vantagem de existir."
print('A letra r aparece %i vezes.\n' %(frase.count('r')) )


# # Fim

# ### Obrigado - Data Science Academy - <a href="http://facebook.com/dsacademybr">facebook.com/dsacademybr</a>
