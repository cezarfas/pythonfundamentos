# Calculadora em Python

# Desenvolva uma calculadora em Python com tudo que você aprendeu nos capítulos 2 e 3. 
# A solução será apresentada no próximo capítulo!
# Assista o vídeo com a execução do programa!

def opera(op,a,b):
    '''Faz as operacoes matemáticas.'''
    ops = list(range(1,5))
    if opcao in ops: #Verifica se a entrada esta correta, entre 1 e 5.
        if opcao == 1:
            resp = a+b
            simbolo = ' + '
        elif opcao == 2:
            resp = a-b
            simbolo = ' - '
        elif opcao == 3:
            resp = a*b
            simbolo = ' * '
        else:
            resp = a/b
            simbolo = ' / '
    else:
        print("\nOpção inválida. Tente novamente.\n")
    print("\n\n ",a,simbolo,b," = ",resp,"\n\n")
    return

print("\n******************* Python Calculator *******************")

print('\nSelecione a operação desejada:\n\n \
        1 - Soma\n \
        2 - Subtração\n \
        3 - Multiplicação\n \
        4 - Divisão\n')

opcao = int(input('\nDigite sua opção:')) # Leitura e conversao de string para int.
n1 = float(input('\nDigite o primeiro número:')) # ... de string para float
n2 = float(input('\nDigite o segundo número:'))

opera(opcao,n1,n2)
